from django.shortcuts import HttpResponse, render

# Create your views here.

def acercade(request):
    return render(request,'Plantceib/acercade.html')

def index(request):
    return render(request,'Plantceib/index.html')  

def contacto(request):
    return render(request,'Plantceib/contacto.html')      
